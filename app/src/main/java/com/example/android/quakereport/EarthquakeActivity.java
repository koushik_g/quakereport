package com.example.android.quakereport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.preference.PreferenceManager;

import java.util.List;

public class EarthquakeActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<EarthquakeDetailsModel>> {
    private ListView earthquakeListView;
    private EarthquakesAdapter adapter;
    private TextView noEarthQuakeView;
    private ProgressBar progressBar;

    public static final String LOG_TAG = EarthquakeActivity.class.getName();
    private static final String BASE_URL = "https://earthquake.usgs.gov/fdsnws/event/1/query";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earthquake_activity);

        noEarthQuakeView = findViewById(R.id.noEarthQuake);
        progressBar = findViewById(R.id.loading_spinner);
        earthquakeListView = (ListView) findViewById(R.id.list);
        earthquakeListView.setEmptyView(noEarthQuakeView);
        triggerTheLoader();
    }

    private void setListeners(List<EarthquakeDetailsModel> earthquakeDetailsModels) {
        adapter = new EarthquakesAdapter(this, earthquakeDetailsModels);
        earthquakeListView.setAdapter(adapter);

        earthquakeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EarthquakeDetailsModel item = adapter.getItem(position);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getmUrl()));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }

    @NonNull
    @Override
    public Loader<List<EarthquakeDetailsModel>> onCreateLoader(int id, @Nullable Bundle args) {
        Log.i(LOG_TAG, "onCreateLoader method called");
        return new EarthQuakeLoader(EarthquakeActivity.this, getFullUri());
    }

    private String getFullUri() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String magnitude = sharedPrefs.getString(
                getString(R.string.settings_min_magnitude_key),
                getString(R.string.settings_min_magnitude_default));
        String orderBy = sharedPrefs.getString(
                getString(R.string.settings_order_by_key),
                getString(R.string.settings_order_by_default));
        Uri baseUri = Uri.parse(BASE_URL);
        Uri.Builder uriBuilder = baseUri.buildUpon();

        uriBuilder.appendQueryParameter("format", "geojson");
        uriBuilder.appendQueryParameter("limit", "10");
        uriBuilder.appendQueryParameter("minmag", magnitude);
        uriBuilder.appendQueryParameter("orderby", orderBy);

        return uriBuilder.toString();
    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<EarthquakeDetailsModel>> loader) {
        Log.i(LOG_TAG, "onLoaderReset method called");
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<EarthquakeDetailsModel>> loader, List<EarthquakeDetailsModel> data) {
        Log.i(LOG_TAG, "onLoadFinished method called");
        Log.i(LOG_TAG, "response" + data);
        progressBar.setVisibility(View.GONE);
        if (null != data && data.size() > 0) {
            setListeners(data);
        } else {
            noEarthQuakeView.setText(R.string.no_earthquakes);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i(LOG_TAG, "onResume called");
        triggerTheLoader();
    }

    private void triggerTheLoader() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        if (isConnected) {
            Log.i(LOG_TAG, "Before initializing loader");
            LoaderManager.getInstance(EarthquakeActivity.this).initLoader(1, null, this);
            Log.i(LOG_TAG, "After initializing loader");
        } else {
            if (null != adapter) {
                adapter.clear();
            }
            progressBar.setVisibility(View.GONE);
            noEarthQuakeView.setText(R.string.no_internet);
        }
    }

}
