package com.example.android.quakereport;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public final class QueryUtils {

    private static final String LOG_TAG = QueryUtils.class.getSimpleName();

    private QueryUtils() {

    }

    public static List<EarthquakeDetailsModel> fetchHttpResponse(String baseUrl) {
        URL url = createUrl(baseUrl);
        String response = "";
        if (null != url) {
            try {
                response = makeHttpRequest(url);
                Log.i(LOG_TAG, "Triggering webservice call");
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error closing input stream", e);
            }
        }
        return parseResponse(response);
    }

    private static URL createUrl(String baseUrl) {
        URL url = null;
        if (!TextUtils.isEmpty(baseUrl)) {
            try {
                url = new URL(baseUrl);
            } catch (MalformedURLException e) {
                Log.e(LOG_TAG, "Error in creating URL: " + e);
            }
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (null != url) {
            HttpURLConnection urlConnection = null;
            InputStream stream = null;
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(15000);
                urlConnection.setReadTimeout(10000);
                urlConnection.connect();
                if (urlConnection.getResponseCode() == 200) {
                    stream = urlConnection.getInputStream();
                    jsonResponse = readStream(stream);
                } else {
                    Log.e(LOG_TAG, "Error receiving the response: " + urlConnection.getResponseCode());
                }
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error making HTTP Request: " + e);
            } finally {
                if (null != urlConnection) {
                    urlConnection.disconnect();
                }
                if (null != stream) {
                    stream.close();
                }
            }
        }

        return jsonResponse;
    }

    private static String readStream(InputStream stream) throws IOException {
        if (null == stream) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        InputStreamReader streamReader = new InputStreamReader(stream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(streamReader);
        String line = bufferedReader.readLine();
        while (null != line) {
            builder.append(line);
            line = bufferedReader.readLine();
        }
        return builder.toString();
    }

    private static List<EarthquakeDetailsModel> parseResponse(String jsonResponse) {
        if (TextUtils.isEmpty(jsonResponse)) {
            return null;
        }

        ArrayList<EarthquakeDetailsModel> quakeReport = new ArrayList<>();
        try {
            JSONObject rootObj = new JSONObject(jsonResponse);
            JSONArray featuresArray = rootObj.optJSONArray("features");
            if (null != featuresArray) {
                for (int i=0; i<featuresArray.length(); i++) {
                    JSONObject featureItem = featuresArray.getJSONObject(i);
                    if (null != featureItem) {
                        JSONObject properties = featureItem.optJSONObject("properties");
                        if (null != properties) {
                            String place = properties.optString("place");
                            double magnitude = properties.optDouble("mag");
                            long time = properties.optLong("time");
                            String url = properties.optString("url");
                            quakeReport.add(new EarthquakeDetailsModel(magnitude, place, time, url));
                        }
                    }
                }
            }

        } catch (JSONException e) {
            Log.e("QueryUtils", "Problem parsing the earthquake JSON results", e);
        }

        return quakeReport;
    }

}
