package com.example.android.quakereport;

public class EarthquakeDetailsModel {
    private double mMagnitude;
    private String mLocation;
    private long mDate;
    private String mUrl;

    public EarthquakeDetailsModel(double mMagnitude, String mLocation, long mDate, String url) {
        this.mMagnitude = mMagnitude;
        this.mLocation = mLocation;
        this.mDate = mDate;
        this.mUrl = url;
    }

    public double getmMagnitude() {
        return mMagnitude;
    }

    public String getmLocation() {
        return mLocation;
    }

    public long getmDate() {
        return mDate;
    }

    public String getmUrl() {
        return mUrl;
    }

    @Override
    public String toString() {
        return "EarthquakeDetailsModel{" +
                "mMagnitude=" + mMagnitude +
                ", mLocation='" + mLocation + '\'' +
                ", mDate=" + mDate +
                ", mUrl='" + mUrl + '\'' +
                '}';
    }
}
