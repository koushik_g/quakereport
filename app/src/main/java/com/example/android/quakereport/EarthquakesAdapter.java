package com.example.android.quakereport;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EarthquakesAdapter extends ArrayAdapter<EarthquakeDetailsModel> {
    private static final String OF_VAL = " of ";
    private Context mContext;

    public EarthquakesAdapter(@NonNull Context context, @NonNull List<EarthquakeDetailsModel> objects) {
        super(context, 0, objects);
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (null == view) {
            view = LayoutInflater.from(mContext).inflate(R.layout.adapter_list_item, parent, false);
        }

        EarthquakeDetailsModel detailsModel = getItem(position);

        TextView magnitude = view.findViewById(R.id.magnitude);
        TextView location = view.findViewById(R.id.location);
        TextView date = view.findViewById(R.id.date);
        TextView time = view.findViewById(R.id.time);
        TextView distance = view.findViewById(R.id.distance);
        GradientDrawable magCircle = (GradientDrawable) magnitude.getBackground();


        if (null != detailsModel) {
            double mag = detailsModel.getmMagnitude();
            int magColor = getMagnitudeColor(mag);
            magCircle.setColor(magColor);
            magnitude.setText(formatDecimal(mag));
            String locVal = detailsModel.getmLocation();
            int index = locVal.indexOf(OF_VAL);
            if (index == -1) {
                distance.setText(R.string.near_the);
                location.setText(locVal);
            } else {
                String valOffset = locVal.substring(0, index+3);
                String valLocation = locVal.substring(index+4);
                distance.setText(valOffset);
                location.setText(valLocation);
            }
            Date dateObj = new Date(detailsModel.getmDate());
            date.setText(formatDate(dateObj));
            time.setText(formatTime(dateObj));
        }

        return view;
    }

    private String formatTime(Date dateObj) {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
        return formatter.format(dateObj);
    }
    private String formatDate(Date dateObj) {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        return formatter.format(dateObj);
    }

    private String formatDecimal(double mag) {
        DecimalFormat formatter = new DecimalFormat("0.0");
        return formatter.format(mag);
    }

    private int getMagnitudeColor(double mag) {
        int modifiedMag = (int) Math.floor(mag);
        int color;
        switch (modifiedMag) {
            case 0:
            case 1:
                color = R.color.magnitude1;
                break;
            case 2:
                color = R.color.magnitude2;
                break;
            case 3:
                color = R.color.magnitude3;
                break;
            case 4:
                color = R.color.magnitude4;
                break;
            case 5:
                color = R.color.magnitude5;
                break;
            case 6:
                color = R.color.magnitude6;
                break;
            case 7:
                color = R.color.magnitude7;
                break;
            case 8:
                color = R.color.magnitude8;
                break;
            case 9:
                color = R.color.magnitude9;
                break;
            default:
                color = R.color.magnitude10plus;
                break;
        }
        return ContextCompat.getColor(mContext, color);
    }
}
