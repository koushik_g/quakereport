package com.example.android.quakereport;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.content.AsyncTaskLoader;


import java.util.List;

public class EarthQuakeLoader extends AsyncTaskLoader<List<EarthquakeDetailsModel>> {
    public static final String LOG_TAG = EarthQuakeLoader.class.getName();
    private String url;
    private List<EarthquakeDetailsModel> result = null;

    public EarthQuakeLoader(@NonNull Context context, String url) {
        super(context);
        this.url = url;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        Log.i(LOG_TAG, "onStartLoading method called");
        if (takeContentChanged() || this.result == null || this.result.size() == 0) {
            forceLoad();
        } /*else {
            deliverResult(result);
        }*/
    }

    @Nullable
    @Override
    public List<EarthquakeDetailsModel> loadInBackground() {
        Log.i(LOG_TAG, "loadInBackground method called");
        if (!TextUtils.isEmpty(url)) {
            result = QueryUtils.fetchHttpResponse(url);
            return result;
        } else {
            return null;
        }
    }

    /*@Override
    public void deliverResult(@Nullable List<EarthquakeDetailsModel> data) {
        super.deliverResult(data);
    }*/
}
